#!/usr/bin/env python3

import requests
import re
from datetime import datetime


ARISS_NEWS_URL = "http://www.amsat.org/amsat/ariss/news/arissnews.txt"
regex = r"Contact is a go for: (.{3}) (\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2} UTC) (\d{2} deg).*"

r = requests.get(ARISS_NEWS_URL)
ariss_news_html = r.text
matches = re.finditer(regex, ariss_news_html, re.MULTILINE)

dates = []
for num, match in enumerate(matches):
    start = datetime.strptime(match.group(2), "%Y-%m-%d %H:%M:%S UTC")
    dates.append(start)
    # contacts.append({'date': datetime(matches.group(1))})

for start in sorted(set(dates)):
    print(start)
